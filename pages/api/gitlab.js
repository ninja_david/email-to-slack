// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios';
import DomParser from 'dom-parser';
import TurndownService from 'turndown';

const parser = new DomParser();
const turndownSvc = new TurndownService();
turndownSvc.addRule('links', {
    filter: ['a'],
    replacement: (content, node) => {
        return `<${node.href}|${content}>`;
    }
});
turndownSvc.remove(['meta', 'title', 'style']);

const blockSplitter = new RegExp(/\n\n/);
const imgMarkdownSplitter = new RegExp(/(?<=!\[.*]\(.*\))/);
const imgMarkdownParser = new RegExp(/!\[(?<alt_text>.*)]\((?<image_url>.*)\)/);

export default async (req, res) => {
    const blocks = [];
    let title;
    try {
        const html = parser.parseFromString(req.body, 'text/html');
        const body = html.getElementsByTagName('body')[0];
        title = body.getElementsByTagName('title')[0]?.innerHTML ?? 'No Title';
        const content = body.getElementsByClassName('content')[0].innerHTML ?? 'No content';
        const md = turndownSvc.turndown(content).trim();
        md.split(blockSplitter).forEach((block) => {
            const elements = [];
            block.split(imgMarkdownSplitter).forEach((chunk) => {
                let text = chunk.trim();
                let img;
                const matches = chunk.match(imgMarkdownParser);
                if (matches) {
                    const [imgMd, alt_text, image_url] = matches;
                    text = chunk.replace(imgMd, '').trim();
                    img = {type: 'image', image_url, alt_text};
                }
                if (text.length > 0) {
                    elements.push({type: 'mrkdwn', text});
                }
                if (img) {
                    elements.push(img);
                }
            });
            if (elements.length > 0) {
                blocks.push({type: 'context', elements});
            }
        });
        let blocksJSON = JSON.stringify(blocks);

        await axios.post(
            'https://hooks.slack.com/services/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',
            {
                channel: '#david-gitlab',
                username: 'GitLab',
                icon_emoji: ':gitlab:',
                text: title,
                blocks,
            }
        );
    } catch (e) {
        console.error("Error!", e);
        return res.status(500).json({});
    }

    res.status(200).json({title, blocks});
}
